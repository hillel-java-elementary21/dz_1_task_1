package com.company;

public class Human {
    private String firstName;
    private String secondName;
    private String patronymic;

    public Human(String secondName, String firstName) {
        this.secondName = secondName;
        this.firstName = firstName;
    }

    public Human(String secondName, String firstName, String patronymic) {
        this.secondName = secondName;
        this.firstName = firstName;
        this.patronymic = patronymic;
    }

    public String getFullName() {
        StringBuilder result = new StringBuilder();
        if (patronymic == null) {
            result.append(secondName).append(" ").append(firstName);
        } else {
            result.append(secondName).append(" ").append(firstName).append(" ").append(patronymic);
        }
        return result.toString();
    }

    public String getShortName() {
        StringBuilder result = new StringBuilder();
        if (patronymic == null) {
            result.append(secondName).append(" ").append(firstName.charAt(0)).append(".");
        } else {
            result.append(secondName).append(" ").append(firstName.charAt(0)).append(". ").append(patronymic.charAt(0))
                    .append(".");
        }
        return result.toString();
    }
}
