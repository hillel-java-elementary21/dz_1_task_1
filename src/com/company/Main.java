package com.company;

public class Main {

    public static void main(String[] args) {
        Human human1 = new Human("Белый", "Игорь", "Владимирович");
        Human human2 = new Human("Белый", "Игорь");
        System.out.println(human1.getFullName());
        System.out.println(human2.getFullName());
        System.out.println(human1.getShortName());
        System.out.println(human2.getShortName());
    }
}

